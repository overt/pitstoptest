//
//  PTPointOfInterestService.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    static let PTPointOfInterestServiceDidUpdatePointsOfInterest = NSNotification.Name("PTPointOfInterestServiceDidUpdatePointsOfInterest")
}

extension PTPointOfInterestService {
    enum PTPointOfInterestServiceError: Swift.Error {
        case unknown
    }
}

class PTPointOfInterestService : PTService {
    
    private var pointsOfInterestStorage: [PTPointOfInterest] = []
    
    override func servicesDidInit() {
        self.loadFromCache()
        self.updateFromNetwork()
    }
    
    func updateFromNetwork(completion: ((Error?) -> Void)? = nil) {
        self.serviceProvider?.APIService.fetchPointsOfInterest(completion: { (aPOIs, anError) in
            if (nil != aPOIs) && (nil == anError) {
                self.pointsOfInterestStorage = aPOIs!
                
                self.saveToCache()
                NotificationCenter.default.post(name: .PTPointOfInterestServiceDidUpdatePointsOfInterest, object: self)
                if (nil != completion) {
                    completion!(nil)
                }
            } else {
                if (nil != completion) {
                    completion!((nil != anError) ? anError : PTPointOfInterestServiceError.unknown)
                }
            }
        })
    }
    
    func pointsOfInterest() -> [PTPointOfInterest] {
        return self.pointsOfInterestStorage
    }
}

//MARK: - cache

extension PTPointOfInterestService {
    private struct Cache: Codable {
        static let cacheVersion = "1"
        
        let currentVersion: String
        let pointsOfInterest: [PTPointOfInterest]
        
        init(pointsOfInterest: [PTPointOfInterest]) {
            self.currentVersion = Cache.cacheVersion
            self.pointsOfInterest = pointsOfInterest
        }
    }
    
    fileprivate func saveToCache() {
        guard let url = self.cacheURL() else {
            return
        }
        
        let cache = Cache(pointsOfInterest: self.pointsOfInterestStorage)
        let data = try? JSONEncoder().encode(cache)
        
        try? data?.write(to: url, options: .atomicWrite)
    }
    
    fileprivate func loadFromCache() {
        guard let url = self.cacheURL() else {
            return
        }
        
        guard let data = try? Data(contentsOf: url) else {
            return
        }
        
        guard let cache = try? JSONDecoder().decode(Cache.self, from: data) else {
            return
        }
        
        if (cache.currentVersion != Cache.cacheVersion) {
            return
        }
        
        self.pointsOfInterestStorage = cache.pointsOfInterest
        NotificationCenter.default.post(name: .PTPointOfInterestServiceDidUpdatePointsOfInterest, object: self)
    }
    
    fileprivate func clearCache() {
        guard let url = self.cacheURL() else {
            return
        }
        
        try? FileManager.default.removeItem(at: url)
    }
    
    private func cacheURL() -> URL? {
        let cacheUrl =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first! as NSURL
        let folder = cacheUrl.appendingPathComponent("pointsOfInterest", isDirectory: true)
        
        if (nil != folder) {
            if(!FileManager.default.fileExists(atPath: folder!.path)) {
                try? FileManager.default.createDirectory(at: folder!, withIntermediateDirectories: true)
            }
        }
        
        return folder?.appendingPathComponent("pointsOfInterest.cache", isDirectory: false)
    }
}
