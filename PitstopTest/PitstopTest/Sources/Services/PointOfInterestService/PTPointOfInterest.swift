//
//  PTPointOfInterest.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import Foundation

class PTPointOfInterest: Codable {
    
    let title: String
    let description: String
    let address: String
    let imageURL: URL
    let openHours: PTOpenHours
    let placeURL: URL
    
    
    required init(title: String, description: String, address: String, imageURL: URL, openHours: PTOpenHours, placeURL: URL) {
        self.title = title
        self.description = description
        self.address = address
        self.imageURL = imageURL
        self.openHours = openHours
        self.placeURL = placeURL
    }
    
    convenience init?(apiDictionary: [String : Any]) {
        guard let title = apiDictionary["title"] as? String else {
            return nil
        }
        
        guard let description = apiDictionary["description"] as? String else {
            return nil
        }
        
        guard let address = apiDictionary["address"] as? String else {
            return nil
        }
        
        guard let imageURLString = apiDictionary["image"] as? String else {
            return nil
        }
        guard let imageURL = URL(string: imageURLString) else {
            return nil
        }
        
        guard let openHoursDict = apiDictionary["hours"] as? [String : String] else {
            return nil
        }
        guard let openHours = PTOpenHours(apiDictionary: openHoursDict) else {
            return nil
        }
        
        guard let placeURLString = apiDictionary["url"] as? String else {
            return nil
        }
        guard let placeURL = URL(string: placeURLString) else {
            return nil
        }
        
        self.init(title: title, description: description, address: address, imageURL: imageURL, openHours: openHours, placeURL: placeURL)
    }
}
