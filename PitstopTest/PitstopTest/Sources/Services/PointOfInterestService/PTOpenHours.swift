//
//  PTOpenHours.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import Foundation

struct PTOpenHours: Codable {
    
    private let datesStorage: [String : String]
    private let timezone: TimeZone
    
    enum Weekday: String, Codable {
        case sunday = "Sunday"
        case monday = "Monday"
        case tuesday = "Tuesday"
        case wednesday = "Wednesday"
        case thursday = "Thursday"
        case friday = "Friday"
        case saturday = "Saturday"
        
        static let count = 7
        static func weekday(index: Int) -> Weekday {
            var result = Weekday.monday
            
            switch index {
            case 0:
                result = .sunday
            case 1:
                result = .monday
            case 2:
                result = .tuesday
            case 3:
                result = .wednesday
            case 4:
                result = .thursday
            case 5:
                result = .friday
            case 6:
                result = .saturday
            default:
                fatalError("Index out of bounds")
            }
            
            return result
        }
        
        static func weekday(calendarWeekdayIndex: Int) -> Weekday {
            guard (1...7 ~= calendarWeekdayIndex) else {
                fatalError("Index out of bounds")
            }
            
            return Weekday.weekday(index: calendarWeekdayIndex - 1)
        }
    }
    
    func isClosed(atDate aDate: Date) -> Bool? {
        var timezoneCalendar = Calendar(identifier: .gregorian)
        timezoneCalendar.timeZone = self.timezone
        
        var gmtCalendar = Calendar(identifier: .gregorian)
        gmtCalendar.timeZone = TimeZone(secondsFromGMT: 0)!
        
        let currentDateWeekdayIndex = timezoneCalendar.component(.weekday, from: aDate)
        let currentDateComponents = timezoneCalendar.dateComponents([.hour, .minute], from: aDate)
        
        guard let currentDate = gmtCalendar.date(byAdding: currentDateComponents, to: Date(timeIntervalSince1970: 0.0)) else {
            return nil
        }
        
        let hoursInfoString = self.hoursInfoString(weekday: Weekday.weekday(calendarWeekdayIndex: currentDateWeekdayIndex))
        
        if (hoursInfoString.lowercased() == "closed") {
            return true
        }
        
        let components = hoursInfoString.components(separatedBy: "-")
        guard 2 == components.count else {
            return nil
        }
        
        let openString = components.first
        let closeString = components.last
        
        func dateFromString(string: String?) -> Date? {
            guard (nil != string) else {
                return nil
            }
            
            
            let formatter = DateFormatter()
            formatter.calendar = gmtCalendar
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            formatter.defaultDate = Date(timeIntervalSince1970: 0.0)
            formatter.dateFormat = "h:mma"
            
            var date = formatter.date(from: string!)
            if (nil == date) {
                formatter.dateFormat = "ha"
                date = formatter.date(from: string!)
            }
            
            return date
        }
        
        guard let openDate = dateFromString(string: openString) else {
            return nil
        }
        guard let closeDate = dateFromString(string: closeString) else {
            return nil
        }
        
        return (currentDate < openDate) || (currentDate > closeDate)
    }
    
    func hoursInfoString(weekday: Weekday) -> String {
        return self.datesStorage[weekday.rawValue]!
    }
    
    init?(apiDictionary: [String : String]) {
        for iIndex in 0..<Weekday.count {
            guard let _ = apiDictionary[Weekday.weekday(index: iIndex).rawValue] else {
                return nil
            }
        }
        
        self.datesStorage = apiDictionary
        self.timezone = TimeZone(abbreviation: "EST")!
    }
}
