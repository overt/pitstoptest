//
//  PTAPIService+PointOfInterest.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import Foundation
import Alamofire

extension PTAPIService {
    enum PTAPIServicePointOfInterestError: Swift.Error {
        case unknown
    }
}

extension PTAPIService {
    func fetchPointsOfInterest(completion aCompletion: @escaping ([PTPointOfInterest]?, Error?) -> Void) {
        let path = "https://api.myjson.com/bins/1vhe1"
        
        Alamofire.request(
            path,
            method: .get)
            .validate()
            .responseJSON { (response) -> Void in
                
                guard response.result.isSuccess else {
                    aCompletion(nil, PTAPIServicePointOfInterestError.unknown)
                    return
                }

                guard let value = response.result.value as? [[String: Any]] else {
                    aCompletion(nil, PTAPIServicePointOfInterestError.unknown)
                    return
                }

                let pois = value.flatMap({ (iDict) -> PTPointOfInterest? in
                    return PTPointOfInterest(apiDictionary: iDict)
                })

                aCompletion(pois, nil)
        }
    }
}
