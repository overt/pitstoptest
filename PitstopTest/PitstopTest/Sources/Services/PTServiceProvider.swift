//
//  PTServiceProvider.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit

class PTServiceProvider {
    static let sharedProvider = PTServiceProvider()
    
    let pointOfInterestService: PTPointOfInterestService
    let APIService: PTAPIService
    
    init() {
        self.pointOfInterestService = PTPointOfInterestService()
        self.APIService = PTAPIService()
        
        self.pointOfInterestService.serviceProvider = self
        self.APIService.serviceProvider = self
        
        self.pointOfInterestService.servicesDidInit()
        self.APIService.servicesDidInit()
    }
}
