//
//  PTPointOfInterestViewController.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit
import SafariServices
import AlamofireImage
import GoogleMaps

class PTPointOfInterestViewController: UIViewController {
    let pointOfInterest: PTPointOfInterest
    
    private let scrollView = UIScrollView()
    private let photoView = UIImageView()
    private let descriptionLabel = UILabel()
    private let urlButton = UIButton(type: .custom)
    private let mapView = GMSMapView.map(withFrame: .zero, camera: GMSCameraPosition.camera(withLatitude: 0.0, longitude: 0.0, zoom: 1.0))
    private let openingHoursView = PTPointOfInterestOpenHoursView()
    
    init(pointOfInterest: PTPointOfInterest) {
        self.pointOfInterest = pointOfInterest
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButtonImage = UIImage(named: "nav_back_btn")?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backButtonImage,
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(closeBarButtonPressed(_:)))
        self.navigationItem.title = self.pointOfInterest.title
        self.view.backgroundColor = UIColor.white
        
        self.view.addSubview(self.scrollView)
        
        self.photoView.contentMode = .scaleAspectFill
        self.photoView.af_setImage(withURL: self.pointOfInterest.imageURL, placeholderImage: PTDesign.defaultImageViewPlaceholder()) { (_) in
            self.view.setNeedsLayout()
        }
        self.scrollView.addSubview(self.photoView)
        
        self.descriptionLabel.textAlignment = .left
        self.descriptionLabel.font = PTDesign.defaultFont(size: 14.0, weight: .light)
        self.descriptionLabel.textColor = UIColor.darkGray
        self.descriptionLabel.numberOfLines = 0
        self.descriptionLabel.text = self.pointOfInterest.description
        self.scrollView.addSubview(self.descriptionLabel)
        
        self.urlButton.addTarget(self, action: #selector(urlButtonDidPressed(_:)), for: .touchUpInside)
        self.urlButton.setTitle(self.pointOfInterest.placeURL.absoluteString, for: .normal)
        self.urlButton.titleLabel?.font = PTDesign.defaultFont(size: 16.0, weight: .regular)
        self.urlButton.setTitleColor(UIColor.blue, for: .normal)
        self.urlButton.contentHorizontalAlignment = .left
        self.scrollView.addSubview(self.urlButton)
        
        self.openingHoursView.openingHours = self.pointOfInterest.openHours
        self.scrollView.addSubview(self.openingHoursView)
        
        self.mapView.isHidden = true
        self.scrollView.addSubview(self.mapView)
        
        self.updateMapLocation()
        self.view.setNeedsLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let safeAreaInsets = self.view.safeAreaInsets
        
        self.scrollView.frame = self.view.bounds
        
        let photoViewWidth = Double(self.view.bounds.size.width - 2 * 20.0 - 10.0) / 2.0
        let openingHoursViewWidth = photoViewWidth
        self.photoView.frame = CGRect(x: 20.0,
                                      y: 10.0,
                                      width: photoViewWidth,
                                      height: photoViewWidth * Double(self.photoView.image!.size.height / self.photoView.image!.size.width))
        self.openingHoursView.frame = CGRect(x: Double(self.photoView.frame.maxX + 10.0),
                                             y: 10.0,
                                             width: openingHoursViewWidth,
                                             height: PTPointOfInterestOpenHoursView.height(width: openingHoursViewWidth))
        
        let descriptionLabelWidth = Double(self.view.bounds.size.width - 2 * 20.0)
        self.descriptionLabel.frame = CGRect(x: 20.0,
                                             y: Double(max(self.photoView.frame.maxY, self.openingHoursView.frame.maxY) + 10.0),
                                             width: descriptionLabelWidth,
                                             height: Double(self.descriptionLabel.sizeThatFits(CGSize(width: descriptionLabelWidth,
                                                                                                      height: Double.greatestFiniteMagnitude)).height))
        self.urlButton.frame = CGRect(x: 20.0,
                                      y: Double(self.descriptionLabel.frame.maxY + 5.0),
                                      width: Double(self.view.bounds.size.width - 2 * 20.0),
                                      height: 40.0)
        
        self.mapView.frame = CGRect(x: 20.0,
                                    y: Double(self.urlButton.frame.maxY + 10.0),
                                    width: Double(self.view.bounds.size.width - 2 * 20.0),
                                    height: max(150.0,
                                                Double(self.view.bounds.size.height -
                                                        self.urlButton.frame.maxY - 10.0 -
                                                        safeAreaInsets.top - safeAreaInsets.bottom - 20.0)))
        
        self.scrollView.contentSize = CGSize(width: self.view.bounds.size.width, height: self.mapView.frame.maxY + 20.0)
    }
    
    @objc private func closeBarButtonPressed(_ aSender: UIBarButtonItem) {
        if ((nil != self.navigationController) && (1 < self.navigationController!.viewControllers.count)) {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func urlButtonDidPressed(_ aSender: UIButton) {
        let vc = SFSafariViewController(url: self.pointOfInterest.placeURL)
        self.present(vc, animated: true, completion: nil)
    }
    
    private func updateMapLocation() {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(self.pointOfInterest.address) { (aPlacemarks, anError) in
            let placemarkLocationCoordinate = aPlacemarks?.first?.location?.coordinate
            
            self.mapView.clear()
            
            if (nil != placemarkLocationCoordinate) {
                self.mapView.isHidden = false
                
                let marker = GMSMarker()
                marker.position = placemarkLocationCoordinate!
                marker.map = self.mapView
                
                let camera = GMSCameraPosition.camera(withTarget: placemarkLocationCoordinate!, zoom: 15.0)
                self.mapView.camera = camera
            } else {
                self.mapView.isHidden = true
            }
            
            self.view.setNeedsLayout()
        }
    }
}
