//
//  PTPointOfInterestOpenHoursView.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit

class PTPointOfInterestOpenHoursView: UIView {
    private static let labelHeight = 13.0
    private static let closedLabelHeight = 16.0
    
    var openingHours: PTOpenHours? {
        didSet {
            self.updateUI()
        }
    }
    
    private let closedLabel = UILabel()
    private let labels: [UILabel] = {
        var result: [UILabel] = []
        for _ in 0..<PTOpenHours.Weekday.count {
            result.append(UILabel())
        }
        return result
    }()
    
    class func height(width aWidth: Double) -> Double {
        return Double(PTOpenHours.Weekday.count) * PTPointOfInterestOpenHoursView.labelHeight + PTPointOfInterestOpenHoursView.closedLabelHeight
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.onCreate()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.onCreate()
    }
    
    private func onCreate() {
        self.backgroundColor = UIColor.clear
        
        self.closedLabel.textAlignment = .left
        self.closedLabel.font = PTDesign.defaultFont(size: 14.0, weight: .bold)
        self.addSubview(self.closedLabel)
        
        for iLabel in self.labels {
            iLabel.textAlignment = .left
            iLabel.font = PTDesign.defaultFont(size: 11.0, weight: .light)
            iLabel.textColor = UIColor.black
            self.addSubview(iLabel)
        }
        
        self.setNeedsLayout()
    }
    
    private func updateUI() {
        for i in 0..<PTOpenHours.Weekday.count {
            let iLabel = self.labels[i]
            let iWeekday = PTOpenHours.Weekday.weekday(index: i)
            let iHoursString = self.openingHours?.hoursInfoString(weekday: iWeekday)
            
            iLabel.text = iWeekday.rawValue + ": " + ((nil != iHoursString) ? iHoursString! : "")
        }
        
        let isClosed = self.openingHours?.isClosed(atDate: Date())
        if (nil != isClosed) {
            self.closedLabel.isHidden = false
            self.closedLabel.textColor = isClosed! ? UIColor.red : UIColor.green
            self.closedLabel.text = isClosed! ?
                NSLocalizedString("Closed", comment: "point of interest open hours view: closed label") :
                NSLocalizedString("Open", comment: "point of interest open hours view: open label")
        } else {
            self.closedLabel.isHidden = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.closedLabel.frame = CGRect(x: 0.0,
                                        y: 0.0,
                                        width: Double(self.bounds.size.width),
                                        height: PTPointOfInterestOpenHoursView.closedLabelHeight)
        
        var y = Double(self.closedLabel.frame.maxY)
        for iLabel in self.labels {
            iLabel.frame = CGRect(x: 0.0,
                                  y: y,
                                  width: Double(self.bounds.size.width),
                                  height: PTPointOfInterestOpenHoursView.labelHeight)
            y = Double(iLabel.frame.maxY)
        }
    }
}
