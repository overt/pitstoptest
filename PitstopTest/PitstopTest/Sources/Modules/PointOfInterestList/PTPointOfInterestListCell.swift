//
//  PTPointOfInterestListCell.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit
import AlamofireImage

class PTPointOfInterestListCell: UICollectionViewCell {
    var pointOfInterest: PTPointOfInterest? {
        didSet {
            self.updateUI()
        }
    }
    
    private let photoView = UIImageView()
    private let nameLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    class func cellHeight(width aWidth: Double) -> Double {
        return 80.0
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.onCreate()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.onCreate()
    }
    
    private func onCreate() {
        self.contentView.backgroundColor = UIColor.white
        self.contentView.layer.cornerRadius = 3.0
        self.contentView.layer.masksToBounds = true
        
        self.photoView.contentMode = .scaleAspectFill
        self.photoView.layer.cornerRadius = 3.0
        self.photoView.layer.masksToBounds = true
        self.contentView.addSubview(self.photoView)
        
        self.nameLabel.textAlignment = .left
        self.nameLabel.font = PTDesign.defaultFont(size: 14.0, weight: .bold)
        self.nameLabel.textColor = UIColor.black
        self.contentView.addSubview(self.nameLabel)
        
        self.descriptionLabel.textAlignment = .left
        self.descriptionLabel.font = PTDesign.defaultFont(size: 12.0, weight: .light)
        self.descriptionLabel.textColor = UIColor.darkGray
        self.descriptionLabel.numberOfLines = 2
        self.contentView.addSubview(self.descriptionLabel)
        
        self.setNeedsLayout()
    }
    
    private func updateUI() {
        self.nameLabel.text = self.pointOfInterest?.title
        self.descriptionLabel.text = self.pointOfInterest?.description
        
        let placeholder = PTDesign.defaultImageViewPlaceholder()
        self.photoView.af_cancelImageRequest()
        if (nil != self.pointOfInterest?.imageURL) {
            self.photoView.af_setImage(withURL: self.pointOfInterest!.imageURL, placeholderImage: placeholder)
        } else {
            self.photoView.image = placeholder
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.photoView.frame = CGRect(x: 10.0,
                                      y: 10.0,
                                      width: Double(self.bounds.size.height - 2 * 10.0),
                                      height: Double(self.bounds.size.height - 2 * 10.0))
        self.nameLabel.frame = CGRect(x: Double(self.photoView.frame.maxX + 10.0),
                                      y: Double(self.photoView.frame.minY),
                                      width: Double(self.bounds.size.width - 10.0 - self.photoView.frame.maxX - 10.0),
                                      height: 20.0)
        self.descriptionLabel.frame = CGRect(x: Double(self.photoView.frame.maxX + 10.0),
                                             y: Double(self.photoView.frame.maxY - 40.0),
                                             width: Double(self.bounds.size.width - 10.0 - self.photoView
                                                .frame.maxX - 10.0),
                                             height: 40.0)
    }
}
