//
//  PTPointOfInterestListViewController.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit

class PTPointOfInterestListViewController: UIViewController {
    
    private let cellReuseIdentifier = "PTPointOfInterestListCell"
    private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    private var pointsOfInterest: [PTPointOfInterest] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("SIGHTSEEING", comment: "point of interest list screen: nav bar title")
        
        self.view.backgroundColor = PTDesign.defaultBackgroundColor()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.backgroundColor = UIColor.clear
        self.view.addSubview(self.collectionView)
        
        self.collectionView.register(PTPointOfInterestListCell.self, forCellWithReuseIdentifier: self.cellReuseIdentifier)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.pointOfInterestServiceDidUpdatePointsOfInterest(_:)),
                                               name: .PTPointOfInterestServiceDidUpdatePointsOfInterest,
                                               object: nil)
        self.updateFromService()
        self.view.setNeedsLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.collectionView.frame = self.view.bounds;
        
        let flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 10.0
        flowLayout.sectionInset = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 10.0, right: 0.0);
        let cellWidth = Double(self.view.bounds.size.width - 2 * 10.0)
        flowLayout.itemSize = CGSize(width: cellWidth, height: PTPointOfInterestListCell.cellHeight(width: cellWidth))
        flowLayout.invalidateLayout()
    }
    
    func updateFromService() {
        let pois = PTServiceProvider.sharedProvider.pointOfInterestService.pointsOfInterest()
        self.pointsOfInterest = pois
        
        self.collectionView.reloadData()
    }
    
    @objc private func pointOfInterestServiceDidUpdatePointsOfInterest(_ aNotification: Notification) {
        self.updateFromService()
    }
}

//MARK: - UICollectionViewDataSource

extension PTPointOfInterestListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (0 == section) ? self.pointsOfInterest.count : 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: self.cellReuseIdentifier, for: indexPath) as! PTPointOfInterestListCell
        cell.pointOfInterest = self.pointsOfInterest[indexPath.item];
        return cell
    }
}

//MARK: - UICollectionViewDelegate

extension PTPointOfInterestListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.indexPathsForSelectedItems?.forEach({ (iIndexPath) in
            collectionView.deselectItem(at: iIndexPath, animated: false)
        })
        
        let poi = self.pointsOfInterest[indexPath.item];
        let poiVC = PTPointOfInterestViewController(pointOfInterest: poi)
        self.navigationController?.pushViewController(poiVC, animated: true)
    }
}
