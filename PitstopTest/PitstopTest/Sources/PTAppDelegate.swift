//
//  PTAppDelegate.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class PTAppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyDsV11GCJABJb-iG7rmWWrksYOmTMisXtw")
        let _ = PTServiceProvider.sharedProvider
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = .white
        self.window?.rootViewController = PTRootViewController()
        self.window?.makeKeyAndVisible()
        return true
    }
}
