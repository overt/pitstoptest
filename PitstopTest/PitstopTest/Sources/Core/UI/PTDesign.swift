//
//  PTDesign.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/9/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit

class PTDesign {
    enum FontWeight {
        case regular
        case bold
        case light
    }
    
    class func defaultFont(size: Double, weight: FontWeight = .regular) -> UIFont {
        var name = "Helvetica"
        
        switch weight {
        case .bold:
            name = "Helvetica-Bold"
        case .light:
            name = "Helvetica-Light"
        default:
            name = "Helvetica"
        }
        
        return UIFont(name: name, size: CGFloat(size))!
    }
    
    class func defaultBackgroundColor() -> UIColor {
        return UIColor(white: 0.95, alpha: 1.0);
    }
    
    class func defaultImageViewPlaceholder() -> UIImage {
        return UIImage.image(color: UIColor.lightGray, size: CGSize(width: 5.0, height: 5.0))!
    }
}
