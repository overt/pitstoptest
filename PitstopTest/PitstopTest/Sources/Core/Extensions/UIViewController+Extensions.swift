//
//  UIViewController+Extensions.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import Foundation

import UIKit

extension UIViewController {
    
    func lw_addChildViewController(_ aChildController: UIViewController?) {
        guard nil != aChildController else {
            return
        }
        
        self.addChildViewController(aChildController!)
        self.view.addSubview(aChildController!.view)
        aChildController!.didMove(toParentViewController: self)
    }
    
    func lw_removeFromParentViewController() {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
