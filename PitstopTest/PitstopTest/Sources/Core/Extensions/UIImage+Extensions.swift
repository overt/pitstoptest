//
//  UIImage+Extensions.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/9/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit

extension UIImage {
    class func image(color: UIColor, size: CGSize) -> UIImage? {
        guard size != CGSize.zero else {
            return nil
        }
        
        let scale = UIScreen.main.scale
        let width = ceil(size.width * scale)
        let height = ceil(size.height * scale)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: nil,
                                width: Int(width),
                                height: Int(height),
                                bitsPerComponent: 8,
                                bytesPerRow: 4 * Int(width),
                                space: colorSpace,
                                bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)
        context?.setFillColor(color.cgColor)
        context?.fill(CGRect(x: 0.0, y: 0.0, width: width, height: height))
        
        guard let cgImage = context?.makeImage() else {
            return nil
        }
        
        let result = UIImage(cgImage: cgImage, scale: scale, orientation: .up)
        return result
    }
}
