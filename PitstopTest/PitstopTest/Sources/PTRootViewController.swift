//
//  PTRootViewController.swift
//  PitstopTest
//
//  Created by Pavel Ievko on 3/8/18.
//  Copyright © 2018 Pavlo Ievko. All rights reserved.
//

import UIKit

class PTRootViewController: UIViewController {
    
    var navVC: UINavigationController?
    var poiListVC: PTPointOfInterestListViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.poiListVC = PTPointOfInterestListViewController()
        self.poiListVC!.view.frame = self.view.bounds
        self.poiListVC!.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.navVC = UINavigationController(rootViewController: self.poiListVC!)
        
        self.lw_addChildViewController(self.navVC)
    }
}
